
/*---------------------------------------------------------------------
 *
 * log_mgr.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Logging Homework 2
 * Description: Library helper to create log files. 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>

/* User */
#include "log_mgr.h"

/*----------------------------- Globals ------------------------------*/

char* logfile = NULL;
int logfile_descriptor = -1;

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs:
 * Outputs:
 * Note:
 * 
----------------------------------------------------------------------*/

int log_event (Levels l, const char *fmt, ...)
{
    char buffer[256];
    char dateBuffer[64];
    va_list args;
    int date_length = 0;
    int buffer_length = 0;
    int write_length = 0;
    time_t t = 0;
    struct tm tm;

    // This should probably just be a flag to signify one has been
    // opened.
    if (logfile_descriptor == -1)
    {
        // Call set_logfile here to get things started.
        if (set_logfile(DEFAULT_LOGFILE) )
        {
            return -1;
        }
    }

    t = time(NULL);
    tm = *localtime(&t);


    va_start (args, fmt);
    buffer_length = vsprintf (buffer, fmt, args);

   // Prepare the level.
   if (0 == l)
   {
    date_length = sprintf (dateBuffer, "%d-%d-%d %d:%d:%d:INFO:", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
   }
   else if (1 == l)
   {
    date_length = sprintf (dateBuffer, "%d-%d-%d %d:%d:%d:WARNING:", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
   }
   else if (2 == l)
   {
    date_length = sprintf (dateBuffer, "%d-%d-%d %d:%d:%d:FATAL:", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
   }
   else
   {
       printf("Error while parsing level.\n");
       return -1;
   }

   // Determine new size
   int newSize = date_length + buffer_length + 1; 

   // Allocate new buffer
   char* newBuffer = (char *)malloc(newSize);

   // do the copy and concat
   strcpy(newBuffer, dateBuffer);
   strcat(newBuffer, buffer);
   strcat(newBuffer, "\n");

    // Write the formatted data to logfile.
    write_length = write (logfile_descriptor, newBuffer, newSize);
    va_end (args);

   // release old buffer
   free(newBuffer);

    // Check if there was an error while writing.
    if (write_length != newSize)
    {
        // The number of written bytes does not match the desired.
        return -1;
    }

    return 0;

}

int set_logfile (const char *logfile_name)
{
    int new_logfile_descriptor = 0;
    int ret = 0;

    mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
    new_logfile_descriptor = creat(logfile_name, mode);

    // Open new logfile and check if successful.
    // If successful close previous logfile.
    if (new_logfile_descriptor)
    {
        if (logfile_descriptor > 0)
        {
            close_logfile();
        }
        logfile_descriptor = new_logfile_descriptor;

        ret = 0;
    }

    // Something went wrong opening new file.
    else
    {
         ret = -1;
    }

    // Return -1 on error.
    return ret;
}

// Close specified logfile if necessary.
void close_logfile (void)
{
    close(logfile_descriptor);
    logfile_descriptor = -1;
}
